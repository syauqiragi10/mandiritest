//
//  CategoryListInteractor.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation
import Alamofire

// MARK: Interactor Input (Presenter -> Interactor)
protocol CategoryListInteractorProtocol {
    
    var presenter: CategoryListPresenterProtocol? { get set }
    
    func getCategoryList()
    
    func getSource(category: String)
}

class CategoryListInteractor: CategoryListInteractorProtocol {

    // MARK: Properties
    var presenter: CategoryListPresenterProtocol?
    
    func getCategoryList() {
        let data: [String] = [
            "business",
            "entertainment",
            "general",
            "health",
            "science",
            "sports",
            "technology"
        ]
        var categoryListData: [Category] = []
        for item in data {
            let category = Category(title: item)
            categoryListData.append(category)
        }
        
        presenter?.interactorDidFetchCategoryList(with: .success(categoryListData))
    }
    
    func getSource(category: String) {
        var parameters: Parameters = [:]
        parameters["category"] = category
        parameters["apiKey"] = APIManager.shared.APIKey
        
        APIClient.shared.request(urlString: "top-headlines/sources", parameters: parameters) { [weak self] jsonSuccess in
            
            var sources: [Source] = []
            for item in jsonSuccess["sources"].arrayValue {
                sources.append(Source(item))
            }
            self?.presenter?.interactorDidFetchSourcesSuccess(with: sources)
        } successFail: { [weak self] jsonFail in
            let status = jsonFail["status"].stringValue
            self?.presenter?.interactorDidFetchSourcesFail(with: status)
        } failure: { [weak self] errorString in
            self?.presenter?.interactorDidFetchSourcesFail(with: errorString)
        }
    }
}
