//
//  CategoryListContract.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation

struct Category: Codable {
    let title: String
}
