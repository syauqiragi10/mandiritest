//
//  SourceEntity.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//

import Foundation
import SwiftyJSON

struct Source {
    let id: String?
    let name: String?
    let description: String?
    let url: String?
    let category: String?
    let language: String?
    let country: String?

    init(_ json: JSON) {
        id = json["id"].string
        name = json["name"].string
        description = json["description"].string
        url = json["url"].string
        category = json["category"].string
        language = json["language"].string
        country = json["country"].string
    }
}
