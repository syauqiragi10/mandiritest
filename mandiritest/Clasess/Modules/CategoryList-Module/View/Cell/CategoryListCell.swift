//
//  CategoryListCell.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//

import UIKit

public class CategoryListCell: UICollectionViewCell {
    
    internal lazy var containerView = UIView()
    internal lazy var titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCellView()
        layoutSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.cornerRadius = 8
    }
    
    private func setupCellView() {
        contentView.addSubview(containerView)
        containerView.backgroundColor = .white
        containerView.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(contentView).inset(UIEdgeInsets.zero)
        }
        containerView.addSubview(titleLabel)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(containerView.snp.top).offset(8)
            make.bottom.equalTo(containerView.snp.bottom).offset(-8)
            make.leading.equalTo(containerView.snp.leading).offset(16)
            make.trailing.equalTo(containerView.snp.trailing).offset(-16)
        }
    }
    
    internal func updateTitle(title: String) {
        titleLabel.text = title
        titleLabel.numberOfLines = 1
    }
    
    internal func setStatus(isActive: Bool) {
        containerView.backgroundColor = isActive ? UIColor(named: "2469a5") : .white
        titleLabel.textColor =  isActive ? .white : UIColor(named: "2469a5")
    }
}
