//
//  SourceCell.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//

import UIKit

class SourceCell: UITableViewCell {

    internal lazy var containerView = UIView()
    internal lazy var titleLabel = UILabel()
    internal lazy var descLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCellView()
        layoutSubviews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.cornerRadius = 8
    }
    
    private func setupCellView() {
        contentView.backgroundColor = UIColor(named: "f2f2f2")
        contentView.addSubview(containerView)
        containerView.backgroundColor = .white
        containerView.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(contentView).inset(UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16))
        }
        containerView.addSubview(titleLabel)
        titleLabel.textColor = .black
        titleLabel.font = .boldSystemFont(ofSize: 14)
        titleLabel.textAlignment = .left
        titleLabel.numberOfLines = 0
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(containerView.snp.top).offset(8)
            make.leading.equalTo(containerView.snp.leading).offset(16)
            make.trailing.equalTo(containerView.snp.trailing).offset(-16)
        }
        
        containerView.addSubview(descLabel)
        descLabel.textColor = .gray
        descLabel.font = .systemFont(ofSize: 12)
        descLabel.textAlignment = .left
        descLabel.numberOfLines = 0
        descLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.bottom.equalTo(containerView.snp.bottom).offset(-8)
            make.leading.equalTo(containerView.snp.leading).offset(16)
            make.trailing.equalTo(containerView.snp.trailing).offset(-16)
        }
    }
    
    internal func bindData(title: String, desc: String) {
        titleLabel.text = title
        descLabel.text = desc
    }
}
