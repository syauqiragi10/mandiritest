//
//  CategoryListPresenter.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation

// MARK: View Input (View -> Presenter)
protocol CategoryListPresenterProtocol {
    
    var view: CategoryListViewControllerProtocol? { get set }
    var interactor: CategoryListInteractorProtocol? { get set }
    var router: CategoryListRouterProtocol? { get set }
    
    func fetchCategoryList()
    
    func interactorDidFetchCategoryList(with result: Result<[Category], Error>)
    
    func fetchSources(category: String)
    
    func interactorDidFetchSourcesSuccess(with result: [Source])
    
    func interactorDidFetchSourcesFail(with result: String)
    
    func didSelectSource(source: Source)
}

class CategoryListPresenter: CategoryListPresenterProtocol {
    // MARK: Properties
    var view: CategoryListViewControllerProtocol?
    var interactor: CategoryListInteractorProtocol?
    var router: CategoryListRouterProtocol?
    
    func fetchCategoryList() {
        interactor?.getCategoryList()
    }
    
    func interactorDidFetchCategoryList(with result: Result<[Category], Error>) {
        switch result {
        case .success(let categoryList):
            view?.update(with: categoryList)
        case .failure( _):
            view?.update(with: "Something went wrong")
        }
    }
    
    func fetchSources(category: String) {
        interactor?.getSource(category: category)
    }
    
    func interactorDidFetchSourcesSuccess(with result: [Source]) {
        view?.update(with: result)
    }
    
    func interactorDidFetchSourcesFail(with result: String) {
        view?.update(with: result)
    }
    
    func didSelectSource(source: Source) {
        guard let view = view else {
            return
        }
        router?.pushToArticles(on: view, with: source)
    }
}
