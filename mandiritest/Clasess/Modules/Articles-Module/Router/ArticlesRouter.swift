//
//  ArticlesRouter.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation
import UIKit

typealias ArticlesEntryPoint = ArticlesViewControllerProtocol & ArticlesViewController

// MARK: Router Input (Presenter -> Router)
protocol ArticlesRouterProtocol {
    var entryPoint: ArticlesEntryPoint? { get }
    
    static func start() -> ArticlesRouterProtocol
    
    func pushToDetailArticle(on view: ArticlesViewControllerProtocol, with article: Article)
}

class ArticlesRouter: ArticlesRouterProtocol {
    var entryPoint: ArticlesEntryPoint?
    
    static func start() -> ArticlesRouterProtocol {
        let router = ArticlesRouter()
        
        // MARK: Assign VIP
        var view: ArticlesViewControllerProtocol = ArticlesViewController()
        var presenter: ArticlesPresenterProtocol = ArticlesPresenter()
        var iteractor: ArticlesInteractorProtocol = ArticlesInteractor()
        
        view.presenter = presenter
        iteractor.presenter = presenter
        
        presenter.router = router
        presenter.view = view
        presenter.interactor = iteractor
        
        router.entryPoint = view as? ArticlesEntryPoint
        
        return router
    }
    
    func pushToDetailArticle(on view: ArticlesViewControllerProtocol, with article: Article) {
        let articlesVC = DetailArticleRouter.start()
        guard let initialVC = articlesVC.entryPoint else {
            return
        }
        initialVC.detailArticle = article
        let viewController = view as! ArticlesViewController
        viewController.navigationController?.pushViewController(initialVC, animated: true)
    }
}
