//
//  ArticlesInteractor.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation
import Alamofire

// MARK: Interactor Input (Presenter -> Interactor)
protocol ArticlesInteractorProtocol {
    
    var presenter: ArticlesPresenterProtocol? { get set }
    
    func getArticles(source: String, keyword: String, page: Int)
}

class ArticlesInteractor: ArticlesInteractorProtocol {

    // MARK: Properties
    var presenter: ArticlesPresenterProtocol?
    
    func getArticles(source: String, keyword: String, page: Int) {
        var parameters: Parameters = [:]
        parameters["sources"] = source
        parameters["apiKey"] = APIManager.shared.APIKey
        parameters["q"] = keyword
        parameters["pageSize"] = 10
        parameters["page"] = page
        
        APIClient.shared.request(urlString: "everything", parameters: parameters) { [weak self] jsonSuccess in
            var article: [Article] = []
            for item in jsonSuccess["articles"].arrayValue {
                article.append(Article(item))
            }
            self?.presenter?.interactorDidFetchArticlesSuccess(with: article)
        } successFail: { [weak self] jsonFail in
            let status = jsonFail["status"].stringValue
            self?.presenter?.interactorDidFetchArticlesFail(with: status)
        } failure: { [weak self] errorString in
            self?.presenter?.interactorDidFetchArticlesFail(with: errorString)
        }
    }
}
