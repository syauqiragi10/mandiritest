//
//  ArticlesViewController.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import SnapKit
import UIKit
import Kingfisher

// MARK: View Output (Presenter -> View)
protocol ArticlesViewControllerProtocol {
    var presenter: ArticlesPresenterProtocol? { get set }
    
    func update(with Articles: [Article])
    func update(with error: String)
}

class ArticlesViewController: UIViewController, ArticlesViewControllerProtocol {
    var presenter: ArticlesPresenterProtocol?
    var articles: [Article] = []
    var searchArticles: [Article] = []
    var source: Source? = nil
    var page: Int = 1
    var keyword: String = ""
    var isLoading: Bool = false
    
    private let backImageView: UIImageView = {
        let image = UIImage(named: "ic_back")
        var imageView = UIImageView()
        imageView.image = image
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        var label = UILabel()
        label.font = .boldSystemFont(ofSize: 20)
        label.text = "Articles"
        label.textColor = .white
        label.textAlignment = .right
        return label
    }()
    
    private let sourceLabel: UILabel = {
        var label = UILabel()
        label.font = .boldSystemFont(ofSize: 28)
        label.text = "Source"
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = .left
        return label
    }()
    
    private let searchTextField: UITextField = {
        let search = UITextField()
        search.borderStyle = .roundedRect
        return search
    }()
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.backgroundColor = UIColor(named: "2469a5")
        table.isHidden = true
        table.register(ArticleCell.self, forCellReuseIdentifier: "cell")
        table.separatorStyle = .none
        return table
    }()
    
    private let loadingView: UIActivityIndicatorView = {
        let loading = UIActivityIndicatorView()
        loading.color = UIColor.blue
        return loading
    }()
    
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        loadArticles(keyword: "")
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}

extension ArticlesViewController {
    private func setupView() {
        view.backgroundColor = UIColor(named: "2469a5")
        
        view.addSubview(backImageView)
        backImageView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(8)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.height.equalTo(16)
            make.width.equalTo(24)
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapBack))
        backImageView.isUserInteractionEnabled = true
        backImageView.addGestureRecognizer(tapGestureRecognizer)
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.bottom.equalTo(backImageView.snp.bottom)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
        }
        
        view.addSubview(sourceLabel)
        sourceLabel.text = source?.name ?? ""
        sourceLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(backImageView.snp.bottom).offset(16)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
        }
        
        view.addSubview(searchTextField)
        searchTextField.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(sourceLabel.snp.bottom).offset(16)
            make.leading.equalTo(view.snp.leading).offset(16)
            make.trailing.equalTo(view.snp.trailing).offset(-16)
        }
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: searchTextField.frame.height))
        btn.backgroundColor = UIColor.clear
        btn.setImage(UIImage(named: "ic_search"), for: .normal)
        btn.setTitle("  ", for: .normal)
        searchTextField.rightView = btn
        searchTextField.rightViewMode = .always
        searchTextField.placeholder = "Search Article"
        searchTextField.addTarget(self, action: #selector(onSearch), for: .editingChanged)
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(searchTextField.snp.bottom).offset(8)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
        }
        
        view.addSubview(loadingView)
        loadingView.snp.makeConstraints { make in
            make.centerY.equalTo(view.snp.centerY)
            make.centerX.equalTo(view.snp.centerX)
        }
    }
    
    @objc func onTapBack(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func onSearch() {
        guard let keyword = searchTextField.text else {
            return
        }
        self.keyword = keyword
        loadArticles(keyword: keyword)
        tableView.reloadData()
    }
    
    internal func loadArticles(keyword: String) {
        if !isLoading {
            isLoading = true
            loadingView.startAnimating()
            presenter?.fetchArticles(source: source?.id ?? "", keyword: keyword, page: page)
        }
    }
}

extension ArticlesViewController {
    // MARK: - ArticlesViewControllerProtocol
    func update(with error: String) {
        isLoading = false
        loadingView.stopAnimating()
    }
    
    func update(with articles: [Article]) {
        isLoading = false
        loadingView.stopAnimating()
        if page == 1 {
            self.articles = []
        }
        self.articles.append(contentsOf: articles)
        tableView.reloadData()
        tableView.isHidden = false
    }
    
}

extension ArticlesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if articles.count == 0 {
            return 1
        }
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == articles.count - 3, !isLoading {
            page += 1
            loadArticles(keyword: keyword)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if articles.count == 0 {
            return tableView.frame.height - 56
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if articles.count == 0 {
            let cell = UITableViewCell()
            cell.contentView.backgroundColor = UIColor(named: "2469a5")
            cell.textLabel?.text = "Data Not Found"
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = .boldSystemFont(ofSize: 24)
            cell.textLabel?.textAlignment = .center
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleCell
        cell.selectionStyle = .none
        let data = articles[indexPath.row]
        cell.bindData(title: data.title ?? "", desc: data.description ?? "", urlImage: data.urlToImage ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = articles[indexPath.row]
        presenter?.didSelectArticle(article: data)
    }
}
