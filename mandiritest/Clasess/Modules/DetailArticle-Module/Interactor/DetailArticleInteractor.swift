//
//  DetailArticleInteractor.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//  
//

import Foundation
import Alamofire

// MARK: Interactor Input (Presenter -> Interactor)
protocol DetailArticleInteractorProtocol {
    
    var presenter: DetailArticlePresenterProtocol? { get set }
}

class DetailArticleInteractor: DetailArticleInteractorProtocol {

    // MARK: Properties
    var presenter: DetailArticlePresenterProtocol?
}
