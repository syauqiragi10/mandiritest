//
//  AppDelegate.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let navigationController = CategoryListRouter.createModule()
        
        window = UIWindow()
        window?.backgroundColor = .white
        window?.rootViewController = navigationController
        navigationController.navigationBar.isTranslucent = false
        navigationController.isNavigationBarHidden = true
        window?.makeKeyAndVisible()
        
        return true
    }
}

