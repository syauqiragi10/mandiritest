//
//  APIClient.swift
//  mandiritest
//
//  Created by Ahmad Syauqi Albana on 22/07/22.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIClient {
    
    var baseURL: URL?
    
    static let shared = { APIClient(baseUrl: APIManager.shared.baseURL) }()
    
    required init(baseUrl: String){
        self.baseURL = URL(string: baseUrl)
    }
    
    func getRequest<T: Decodable>(urlString: String,
                                  type: T.Type = T.self,
                                  success: @escaping (Int) -> (),
                                  failure: @escaping (Int) -> ()) {
        
        
        guard let url = URL(string: urlString, relativeTo: baseURL) else {
            return
        }
        let urlString = url.absoluteString
        
        AF.request(urlString, method: .get).responseDecodable(of: type) { response in
            debugPrint("Response: \(response)")
        }
        
    }
    
    func request(urlString: String,
                 parameters: Parameters,
                 success: @escaping (JSON) -> (),
                 successFail: @escaping (JSON) -> (),
                 failure: @escaping (String) -> ()) {
        
        guard let url = URL(string: urlString, relativeTo: baseURL) else {
            return
        }
        
        AF.request(url.absoluteString, method: .get, parameters: parameters)
            .responseData { responseData in
                guard let response = responseData.response, let data = responseData.value else {
                    print(urlString, "=", responseData.response?.statusCode ?? "")
                    return
                }
//                print(url.absoluteString, "=", response.statusCode, parameters)
                
                guard let jsonResult = try? JSON(data: data) else {
                    print("failing jsonData")
                    return
                }
//                print(jsonResult)
                switch responseData.result {
                case .success( _):
                    switch response.statusCode {
                    case 200:
                        success(jsonResult)
                    default:
                        successFail(jsonResult)
                    }
                case .failure(let error) :
                    failure(error.localizedDescription)
//                    print(error.localizedDescription)
                }
            }
        
    }
}
