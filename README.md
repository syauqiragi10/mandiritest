# Mandiri Test

Mobile iOS Application News using API from https://newsapi.org
1. Programming languages: Swift 5.6.1
2. Use VIPER design pattern
3. Use the latest technology/framework.
4. 3rd party library use : 'Alamofire', 'Kingfisher', 'SwiftyJSON', 'SnapKit'
